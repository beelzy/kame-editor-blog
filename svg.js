module.exports = {
    arrow: (eleventy, reverse = false) => {
        return `<svg ${reverse ? 'class="reverse"' : ''}viewBox=" 0 0 477.175 477.175">
            <use href="${eleventy.url('/icons/arrow.svg#arrow')}" width="477px" height="477px">
            </use>
        </svg>`;
    }
}
