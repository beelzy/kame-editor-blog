---
layout: post.11ty.js
title: Devlog is back
tags: post
date: 2021-03-27 00:00:00.00
---

So you may have noticed that the devlog link from the homepage hasn't been working for awhile since the platform that was hosting it has been down for awhile. Unfortunately, I have no idea if or when they'll be up again, so I've decided to move the devlog to a gitlab page, like the main homepage for the editor itself. Unfortunately, because the other platform went down without any notice, I haven't been able to obtain a copy of the previous blog posts, so I'll be unable to restore those here, unfortunately. However, I can provide a summary of some of the previous blog posts, based off of previous changelogs and releases from the GBATemp thread and what I remember happening. There weren't so many posts that it would have been too difficult or time consuming to remember them all. If anybody has an archive of it or some of the posts from before, please do let me know using the contact links from the main homepage.
