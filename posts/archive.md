---
layout: post.11ty.js
title: Summary of Previous Posts
tags: post
date: 2021-03-27 00:30:00.00
---

Because I no longer have a copy of my previous devlog posts, I've now summarized some of the posts from before, or what I remember of them. They are in chronological order, from latest to earliest.

## Kame Editor v1.2.8

I've been pretty busy and haven't had much time to do much with Kame Editor, but I've been able to at least fix some bugs that have come up. Unfortunately, I didn't amass enough changes for there to be another release worth making, so they've sort of just been sitting there. But I have had someone nice enough to provide me with some screenshots to compare what the preview in Kame Editor looks like with the one on the real hardware. Although I wouldn't be able to fix everything in the preview, it helped a lot with some of the rendering and positioning of items on the screen.

Also, it seems a few people got lost with finding out where all the functions in the editor are because I only provided a help key visual for the original 3DS theme, and not the others. So that's been rectified and there's now extra visuals for the other skins if you select them.

Another nice thing that happened was that one of the Mac users reported having issues with converting a particular wav file. Well, it turns out that file had some extra sections and chunks that rstmcpp currently doesn't understand or expect. Upon some extra research, it seems that programs like Logic Pro add these, but it should not hinder or otherwise prevent the wav file from being played. So rstmcpp now correctly ignores those chunks, and you can now convert those files.

I once also got a report from someone about how it crashed on Windows on startup. Unfortunately, I wasn't able to get the user to provide me with much more input about why this was happening; the person who makes the Windows builds also didn't report any crashes like that. The only way in which I got that to happen was if the user was on a system that did not have OpenGL 3.3+. So we decided that instead of having the program crash and tell the user nothing about why that happened, we now do a check for OpenGL version, and if it's too low, we just provide a dialog message and close Kame Editor.

## Downloading Qt5 Requires an Account

It has come to my attention that Qt has changed the way in which they distribute some versions of their software, namely the Qt5 installers. I wondered if this would have an impact on the way I use Qt5 to build and distribute Kame Editor:

* Linux - Not really. On most distros, they often prefer to have their own flavors of the software they package, which means they will often prefer to build Qt from source and choose their own set of flags and configuration. We would typically get Qt5 this way through the package manager instead of from the Qt website or installer, and most distros do not use the installer when making distro packages.
* Mac - When I was deciding how to include Qt5 into the Mac builds for circleCI, I had the choice of building everything from source, grabbing a package from Homebrew or just building Qt5 base and excluding everything else I didn't need. I ended up with the last option, as building everything from source would have taken way too long, and I didn't go for Homebrew as while it was faster, it resulted in bigger build sizes as it includes a bunch of things that Kame Editor doesn't need. The end result is that since I build in a pipeline, an installer where you have to click through everything interferes with the way the pipeline works, and so I went with building just Qt5 base and Qt5 tools from source, and didn't need an account. As for Mac users wanting to build or obtain Qt5 for building Kame Editor, that depends on what you prefer, but you have choice because of things like Homebrew.
* Windows - I don't make the Windows builds, but as far as I can tell, the person who does is fairly comfortable building Qt5 from source, which means they won't need a Qt account either, and can get by downloading the source files. However, Windows users just wanting the prebuilt Qt5 are out of luck and need an account to use the installer. Unless they are okay with building from source, I think they are the users that will be hit hardest by Qt's decision. Maybe it will work with C++ package managers like Conan though, or perhaps Chocolatey has a package for that. No idea how they build the packages though.

So the verdict is: No, Kame Editor is not really affected that much by Qt deciding to force users to get an account to use their installer. I and many others still think it's a bad idea in general though.

## Kame Editor v1.2.7

v1.2.7 is out and I've gotten some feedback from people about some features they might find useful. Some people want to be able to preview the folders without the first letter being displayed, and they also wanted extra theme deploy/import options. Also, I never really considered the idea of branding the preview screenshots; I'm just happy if people like the program and use it. However, I've had users suggest they'd like to see the option of including screenshots with branding in it, so there's now an option that lets you choose whether or not you want to include some text that says the theme was made with Kame Editor.

While Kame Editor mostly just works, there was something I overlooked: jpeg textures. It is possible to convert them so that they're compatible with being used in 3DS themes, but I never really looked that much into it. It works now.

## Kame Editor Autosave and v1.2.6

v1.2.6 is out, and now has some minor bug fixes, and the preview image capture now correctly captures the bottom screen. However, the main special feature with v1.2.6 is autosave. The reason autosave is important isn't necessarily because you don't trust your program not to crash and you don't want users to lose all their work, but because you can't know if your user's OS will behave or not. Sometimes, they might run other programs that cause the crash, or it could even be something their OS is doing itself. In other words, your program can crash through no fault of the program itself, and you can't simply assume that just because you write a program that doesn't crash doesn't mean something else won't cause it to quit unexpectedly. Either way, it still sucks losing all your work, regardless of why it happens, so that's why there is now an autosave feature that automatically saves a version of your theme somewhere in the unlikely event that the program quits unexpectedly, and which you can recover so you don't lose all your work.

## Kame Editor Color Swatches and v1.2.5

In order to complete the whole color palette management concept I had for Kame Editor, the only thing missing is color swatches. So now it's nice that you can share groups of colors between different theme elements, but what if instead of taking the whole group of colors, you only wanted to grab a few of them? Well you can save color swatches too instead of the entire palette. Now that the theme import feature has been completed, I have enough time to go back to the original color management concept. In the color picker, there is now a new section, which if you open up, displays all the colors that you've used recently. And if you liked one of those colors, you can reuse it and select it on another element.

There are some other improvements for v1.2.5, including some minor style changes and fixing an audio bug where if you try to seek to a particular part of the audio, it lands in the middle of the sample and sounds horrible.

## Kame Editor Theme Importing Feature and v1.2.4

Due to user interest, I've had some people ask me if it would be possible to load 3DS theme files from elsewhere into Kame Editor. I've never actually considered this to be that important for Kame Editor, as it's for making themes; not viewing already compiled/deployed ones, although there's nothing that makes this impossible to do, since the formats and methods of creating the theme files are already known; you must just do everything else in reverse. The other reason I didn't touch it for awhile was because I wasn't sure where to dump the resources being extracted from the theme; there will be audio and image assets coming out of the theme file. We've done this now by giving users the option to choose where the files will be saved to.

It has also been brought to my attention that the tooltip in one of the zoom levels of the icons in the bottom screen can actually be hidden. Normally, the two options provided are to use custom theme colors, or to disable those colors and use the default ones. Turns out there's a third option that allows you to disable showing that tooltip completely, in the same zoom level. That's now been fixed.

## Kame Editor Color Palette Management and v1.2.2

I've had on mind the idea for the color palette management feature for awhile, but felt it was something that would be done later in Kame Editor, so I meant to implement it. But due to some other features being of more interest to other users, I've only been able to implement a part of that feature which is the ability to manage colors used in themes. The idea is that there are often a common group of colors that people might feel like reusing across different elements, and it would be tedious to keep selecting the same colors if they're all the same. So now there's an option that allows users to select the colors they want for an element and then save that set to a palette file. Then when you see another element that has similar looking colors as the other element you've already applied colors to, you just load that palette file and it applies those colors to that element. There was supposed to be another color sharing feature that works together with this, but because of the interest in other features in Kame Editor, I never got around to it.

v1.2.2 is also there now with some extra tooltip hints. Also, it turns out that the fpm builds don't work as they should, so I've had to add an extra pipeline in circleCI for Debian builds.

## Kame Editor v1.2.0 and Console Skins

Kame Editor is now at v1.2.0 and has a new feature: Console skins. I actually planned to have console skins in Kame Editor from the beginning, but didn't get around to it until now. The idea is that the console skin provides a mock up of the various types of 3DS/2DS devices. Originally, I hadn't thought it would be useful for people previewing the top and bottom screens together, as there is a gap between them, depending on what version of the 3DS you're on, and just wanted it for the look and feel. But after reading through some of the chats on the Theme Plaza Discord about it, I've realized it was a good idea afterall. The console skin colors aren't meant to be changed though; it's mostly there for layout purposes. And the XL skins also appear smaller, but then I realized they only look smaller because they are displaying everything in the same resolution as the other 3DS consoles. The only reason they appear bigger in real life is because of the pixel sizes. I'm not making an attempt to try to make the console skins be the same size as the physical device, as it would also mean I'd have to make assumptions about the screen resolution and density of whatever device Kame Editor is running on anyways. The idea is only to display the top and bottom screens in the same pixel resolution on all the 3DS devices, and provide a fairly proprtionally accurate layout of the top and bottom screens on any 3DS device you want to see your theme on.

It's also come to my attention that some wavs are formatted in a way that can't be converted by rstmcpp. According to what I could find out about it, there doesn't seem to be much of a difference between using 16 bit or 32 bit channel wavs as the 3DS audio is encoded in such a way that 32 bit channel audio couldn't be supported anyways. So the idea is that you would use Audacity or another audio editing tool to convert any 32 bit channel wavs to 16 or 8 bit before converting them in Kame Editor.

There's a few other improvements in v1.2.0 like being able to autoplay BGM music when they're available and fixing the long SFX warning duration.

## Kame Editor Reaching v1.1.0

Kame Editor has reached v1.1.0 and has some more important changes. The biggest one is that it now displays the 3D folder in the top screen. Before, I wasn't actually sure how I was going to display the 3D folder, although I did want to do it anyways. In order to display the 3D folder, I'd need some library to interpret the 3D data.

Qt does have a library for this called Qt3D, but it adds a lot of libraries and takes up more space just to use, and the only thing I wanted to do with it was change some colors and display the folder; not much else. I didn't want to burden my users with unused libraries in my program taking up space. So I looked to some other alternatives. The one I ended up settling on was a simpler, more barebones one, tinyobjloader. It required a bit of tweaking to get right, but it worked, and now you can see it in the preview.

There is now also a settings widget that allows you to change the language manually and change the deploy path.

## Kame Editor Reaching v1.0.2 and Windows Builds

Kame Editor is now at v1.0.2, and contains some nice changes. One of the features is the ability to preview the pressed states of certain elements while you're using the color picker to change its color. This was added because it's impossible to see the element in its pressed state at the same time you're trying to change its color, so it's automatically pressed while you're changing its color. It's much easier than changing it, pressing it, then changing it again and on and on. Another change is that Kame Editor now has translations. I added a German translation, but it's not my first language, so there may be grammar errors. I encourage others to contribute other translations in other languages or to improve on the German one.

Due to popular demand, it appears that some fraction of Linux users are on Debian or Ubuntu, and aren't comfortable building software from source. So as a quick fix solution, we leveraged fpm to make .deb builds based off of the Archlinux ones I already make.

My original goals for Kame Editor was to provide a 3DS theme editor for Unix users, so it naturally didn't occur to me that Windows users would be interested. There were a few Windows users that were interested, but they were mostly interested for comparison purposes. Thankfully for them, someone on the Nintendo Homebrew Discord channel was interested enough to provide Windows builds and testing, so we have those builds now. However, Windows is not the primary target of Kame Editor, and while we will accept bugs and support them, this is only true so long as there is someone else who is interested in doing the Windows builds.

## Why Qt5

Now that Kame Editor is out, I wanted to explain why I built it with Qt5 and not Electron or any other library or framework. My day job is a frontend web developer, so the natural choice would be Electron, only Electron is essentially a browser running separately for the sole purpose of running an application, and because of the amount of RAM and disk space it takes up, I didn't feel it was a justified choice, and wasn't necessary. Sure, some people consider it a viable option to just throw more resources at the problem, and because people are used to installing programs over 100MB in size, but not if you're a Linux user, where most packages are under 10MB, and throwing more resources at a problem is not a good solution in general. Furthermore, my program targets Unix users, so cross-platform support wasn't as important for me (the irony is that Qt5 is cross platform anyways, but that's not why I picked it.) So I opted to choose using something more native like C/C++ and Qt5. There were other frameworks for displaying graphical user interfaces, including Tk and Gnome, but I decided to stick with Qt5 for the dock widgets feature.

## Kame Editor Reaching v1.0.0

Kame Editor finally reaches v1.0.0, where it has all the features needed to create any theme on the 3DS through homebrew. We also make Mac builds in a pipeline, and there seems to be some issues with OpenGL on some users' computers, so we have an extra flag to disable MSAA. I also have some AUR scripts for building the necessary software on Archlinux, as it's the distro I'm currently using, and it's the easiest to create distribution packages for. At this point, there are still some features I'd like to make with Kame Editor, but it's good enough for a 1.0.0 release.

## The Motivation for Kame Editor and some Branding

The whole reason for making Kame Editor in the first place was because I was disappointed at the lack of support for homebrew 3DS theme tooling on non-Windows systems. There were a few options for that if you weren't on Windows, which involved using outdated or not full-featured tools like Yata+ online, or if you wanted to use Usagi, you had to do it through a virtual machine, and it didn't work properly through Wine. And after looking at Usagi's code, I realized there's no reason why the tools needed to be tied to C# or Windows forms; I could have written all of that in C/C++ and it would have worked out just fine for Unix users. Besides, all the other tools that did similar things or contributed to making 3DS themes were also written in C/C++ and could be built on Unix systems. With the time that an Easter hackathon gave me, I was able to solve most of those problems and could work on a user friendly frontend.

At the beginning, I decided to call the project "Kame" because the currently existing tool on Windows was called Usagi, which is Japanese for rabbit. So I picked the opposite, the Japanese word for turtle, which is Kame. Which is why the logo is a turtle shell and the theme colors are green and black.

## Making Kame Editor Mac Friendly and Solving Audio Playback Issues

I wasn't expecting Kame Editor to take off with a large majority of users; in the first place, it fills a very specific niche, and that niche is made even smaller by the fact that the potential pool of users in that niche are not Windows users, and there are often more Windows users than users of Unix-like systems. But I did manage to get interest in Kame Editor from Mac users, and they did help me with trying to get Kame Editor to work on a Mac. I also got a pipeline through CircleCI because they offer a free variant to people who have free software projects (like Kame Editor), so I was able to make Mac builds that way.

Another issue I encountered was that the audio formats used by the 3DS are not formats that most other audio software on computers understand, so I needed to find some other program that would be able to playback those files. I found out it could be done with vgmstream, but I wanted it to return back a stream and not convert a file, and Qt's provided media player could not do it adequately. It uses GStreamer under the hood, and while it worked on Linux, it had issues with memory leaks, and didn't work at all on Macs. So I had to replace it with an even more low level, but cross platform library, Portaudio. But Portaudio worked fine on both Linux and Mac and didn't have memory leak issues, so I went with it.

## Development Started on Kame Editor

After tackling all the issues with converting formats, I had all the necessary tools needed to create 3DS themes; it only just needed a user friendly frontend. That frontend comes in the form of Kame Editor, which leverages the tools I developed previously; kame-tools and rstmcpp, and a few others to come.

I managed to get a relatively decent looking preview going, and planned for it to be able to display the bottom screen icons in different zoom levels. I wanted the preview to be as complete as possible because it helps users design their themes. The only thing it didn't have at this point was the 3D folder preview. Sure, I could get the 3D object file from Usagi and use it; the problem was how to do this through Qt. There were several ways of doing it, but I wasn't sure which one I wanted to go with, so the matter wasn't settled yet.

## Solving BGM/SFX Issues

Because I didn't have as much time to work on the audio conversion, I ended up tackling it later. There was a lot of information about what headers the audio formats used in the 3DS looked like, but not so much about the decoding. Somebody else had already made an Audacity fork that does just this, so I looked into the code and found a separate library that does this, and doesn't depend on Windows. As for the BGM format, I had to look around in Looping Audio Converter's code and ended up stumbling on rstmcpp, which while it didn't originally do the .cstm format, but did a similar one for the Wii, .rstm, I modified it so that it could do .cstm files as well.

There was another issue when porting over the .cwav conversion on rstmcpp where it wouldn't work for multi channel wav files for awhile. I eventually found out what the error was and made rstmcpp capable of converting all the formats needed for 3DS theme audio.

## Kame Tools is Started as an Easter Hackathon Project

I started doing this thing where I get all this free time during public holidays, and I tend to use that time to work on side projects or try new things. And then I realized they're essentially holiday hackathons. Around 2019, I got the idea to put my 3DS on homebrew because its warranty had already expired, and I wanted to play translation patches of Dai Gyakuten Saiban. And I found out that it would be possible to create your own 3DS themes this way; unfortunately, all of the tooling for doing so was meant for Windows users, and the solution to use Wine didn't work very well, or you had to go the virtualization route.

So I used the Easter holidays in 2019 to research and develop a Unix command line tool that would help with converting the theme resources into the necessary 3DS format required for it to work properly, using banner tools as a base and looking into the source code of Usagi and a few other tools. From perusing through Usagi's code, I realized that there was no reason why a 3DS theme editor needed to be written in C# or needed Windows specific tooling; it could work just as well in other lower level languages like C/C++. I had enough time during the four day Easter weekend to mostly get resources into a 3DS compatible file with almost all of the features working, as well as include sound effects, but didn't have as much time to work with the audio formats or converting them.

I did actually plan to have a more user friendly tool for this, Kame Editor sometime in the future. But in order for there to be a Kame Editor, it needs to be able to convert the resources into the 3DS theme formats, and this was the first step in getting there. I also did this modularly, because it's the "Unix" way; I wanted to be able to give others a chance to provide their own frontends if they so wished to.
