---
layout: post.11ty.js
title: Small but Important Update
tags: post
date: 2024-11-01 19:08:21.00
---

I've had some rare complaints about the bgm being too loud and crackly as well as not fading properly when navigating software banners on real hardware. To be honest, I never really noticed this until it was pointed out to me, and it seems that you have to properly set the sfx header values in the main theme file so that it works properly. It's now been fixed and backported, so now the bgm will be quieter and fade properly when navigating software banners when you use version 1.4.1*. If you're using some other version or build from source, just update kame tools and build again, or sync on AUR on Archlinux.

I don't think the bugfix will be affected by whether or not you use mono or stereo audio or change loop points, but it's recommended to convert to stereo anyways.

* We no longer have access to older mac universal machines on circleci, so we've only been able to update the arm64 builds. You can, however, still build the code from source, or just rebuild kame-tools from source instead and replace the old one that came with the editor.
