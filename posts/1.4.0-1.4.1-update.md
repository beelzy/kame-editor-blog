---
layout: post.11ty.js
title: Kame Editor v1.4.0 and v1.4.1 are here
tags: post
date: 2024-03-29 18:13:55.00
---

I know there haven't been a lot of updates in Kame Editor lately, so you're probably wondering what happened. Well not much, really. I've basically implemented all the features I wanted Kame Editor to have, so any updates now are for maintenance or additional features that other people may request so long as they're in scope and manageable (for example, Kame Editor is a 3DS theme editor, not a general purpose graphics editing software, so we aren't interested in features that allow you to draw all over your screen textures.) Please note that we do try to fix bugs whenever we can, but if they don't end up getting addressed, there can be several reasons for that:
* Your issue doesn't have enough information. If you don't tell me what OS you're on, what version you're using, some screenshots, and consistent ways to reproduce the problem, then it's really difficult to fix the problem, and not worth our time to look at it.
* We lack the necessary hardware to test some of the bugs on. This can be because we don't have access to any recent Apple or Windows devices, or hardware with hiDPI scaling capabilities.
* Other volunteers who might be working on any non-Linux builds are not available. If I can't reproduce the problem, and they aren't available to help me with that, then I can't really fix the problem.
If your issue fits into one of these categories, we're really sorry, but there's not much we can do about it on our end. You might try investigating the issue yourself if you're a developer or if you know someone who is, try to get them interested in it. The source code is freely available and I encourage people to fork or offer pull requests if they have the necessary hardware to fix any of those bugs I'm unable to reproduce on my end. Or if you are on a non-Arch-based distro, you might try and get a maintainer interested in packaging the software, and they might be better able to look at some of the issues that affect their distros but not Arch, and they can open a pull request.

## 1.4.0 - Qt6 Porting

In addition to fixing a few bugs, I've decided to upgrade to Qt6 for good. It seems a lot of newer and older software are doing this now, even if Qt5 is still available, and more stable LTS distros like Debian now have qt6 in their stable repos. So there's no reason not to switch to it; I've kept a qt6 branch around for awhile, and I haven't experienced too many issues with it. It may look slightly different, but otherwise works fine.

## 1.4.1 - Visual SFX Limits

I had a request to display the sfx bar as a sort of graph that indicates visually how much of the size limit your SFX files take up. Previously, the SFX detail panel would tell you what size your audio file is, but it didn't give a particularly good overview of how much space all your SFXes are taking up. So I worked on a feature to display a bar where the size of your SFX files would be displayed visually to tell you how close you are to the limit and maybe which files are taking up the most space. I had hoped to do this using some of Qt's provided graph widgets, but they didn't exactly do what I wanted them to do out of the box, so I ended up just using my own custom widgets for that.

## HiDPI Scaling

Unfortunately, I don't really have a legitimate device on which I can test whether or not HiDPI scaling works properly with Kame Editor. There does appear to be a qt setting to enable it, but I have no idea what will happen. It renders fine on my end at least. It's not enabled in 1.4.0, but it is in 1.4.1, so if it looks strange, or unusable for you, try 1.4.0 instead.
