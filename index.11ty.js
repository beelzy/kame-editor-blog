const striptags = require('striptags');
const svg = require('./svg.js');
const excerpt = (content) => {
    return `${striptags(content).substring(0, 280)}${content.length > 280 ? '...' : ''}`;
};

exports.data = {
    title: 'Home'
    , layout: 'page.11ty.js'
    , pagination: {
        data: 'collections.post'
        , size: 15
        , reverse: true
    }
};

exports.render = (data) => {
    return `<h1>Posts</h1>
        <ul class="entries">
            ${data.pagination.items.map((item) => {
                return `<li><div class="title"><a href="${this.url(item.url)}">${item.data.title}</a><span>${item.data.page.date.toLocaleDateString()}</span></div><hr/><p>${excerpt(item.templateContent)}</p></li>`;
            }).join('')}
            </ul>
        <nav>
        ${data.pagination.href.previous ? `<a href="${this.url(data.pagination.href.previous)}" class="previous">${svg.arrow(this, true)}<span class="button">${data.pagination.reverse ? 'Previous' : 'Next'}</span></a>` : ''}
        <ul>
            ${data.pagination.pages.map((item, index) => {
                return `<li>${data.pagination.pageNumber === index ? `<span>${index + 1}</span>` : `<a href="${this.url(data.pagination.hrefs[index])}">${index + 1}</a>`}</li>`;
            }).join('')}
        </ul>
        ${data.pagination.href.next ? `<a href="${this.url(data.pagination.href.next)}" class="next"><span class="button">${data.pagination.reverse ? 'Next' : 'Previous'}</span>${svg.arrow(this)}</a>` : ''}
    </nav>
        `;
};
