const svg = require('../svg.js');

exports.data = {
    layout: 'page.11ty.js'
};

exports.render = (data) => {
    let collections = data.collections.post.slice().reverse();
    let previousPost = this.getPreviousCollectionItem(collections, data.page);
    let nextPost = this.getNextCollectionItem(collections, data.page);
    return `
        <h1>${data.title}</h1>
        <span class="date">${data.page.date.toLocaleDateString()}</span>
        <article>
            ${data.content}
        </article>
        <nav>
        <a${previousPost ? ` href="${this.url(previousPost.url)}" class="previous">${svg.arrow(this, true)}<span>${previousPost.data.title}</span>` : '>'}</a>
        <a${nextPost ? ` href="${this.url(nextPost.url)}" class="next"><span>${nextPost.data.title}</span>${svg.arrow(this)}` : '>'}</a>
        </nav>
        `;
};
