exports.render = (data) => {
    return `<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Kame Editor Devlog - ${data.title}</title>
        <link href="${this.url('/css/styles.css')}" rel="stylesheet" media="screen">
    </head>
    <body>
        <svg class="bg" viewBox="0 0 140.164 140.386">
            <use href="${this.url('/icons/logo-outline.svg#svg8')}" width="140px" height="140px">
            </use>
        </svg>
        <header>
            <a class="logo" href="https://beelzy.gitlab.io/kame-editor"><img class="logo" src="${this.url('/icons/logo.svg')}"></a>
            <a href="${this.url('/')}">Kame Editor Devlog</a>
        </header>
        <div class="content">
        ${data.content}
        </div>
    </body>
</html>
`;
};
