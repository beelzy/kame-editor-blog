module.exports = (eleventyConfig) => {
    eleventyConfig.addPassthroughCopy('css');
    eleventyConfig.addPassthroughCopy('icons');
    eleventyConfig.addPassthroughCopy('assets');
};
